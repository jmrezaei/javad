from confige import rules, Kavenegar_API_key
from kavenegar import *

def send_sms(text):
    try:
        api = KavenegarAPI(Kavenegar_API_key)
        params = {
            'sender': '1000596446',
            'receptor': rules['notification']['receiver'],
            'message': text,
        }
        response = api.sms_send(params)
        print(response)

    except APIException as e:
        print(e)
    except HTTPException as e:
        print(e)
