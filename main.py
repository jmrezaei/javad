from confige import rules, API_KEY
from notification import send_sms
from datetime import datetime
from khayyam import JalaliDatetime
import json

from fixer import get_rates
from mailgun import send_email_rates


def archive(filename, rates):
    with open(f"archive/{filename}.json", "w") as f:
        f.write(json.dumps(rates))


def check_notify_rules(rates):
    preferred = rules['notification']['preferred']
    msg = ''
    for exc in preferred:
        if rates[exc] <= preferred[exc]['min']:
            msg += f"{exc} reach min: {rates[exc]} \n"
        if rates[exc] >= preferred[exc]['max']:
            msg += f"{exc} reach max: {rates[exc]} \n"
    return msg


def send_notification(msg):
    now = JalaliDatetime(datetime.now()).strftime('%Y-%B-%d %A %H:%M')
    msg += now
    send_sms(msg)


if __name__ == "__main__":
    res = get_rates(API_KEY)
    if rules['archive']:
        archive(res['timestamp'], res['rates'])
    if rules['send_mail']['enable']:
        send_email_rates(res['timestamp'], res['rates'])
    if rules['notification']['enable']:
        msg = check_notify_rules(res['rates'])
        if msg:
            send_notification(msg)
